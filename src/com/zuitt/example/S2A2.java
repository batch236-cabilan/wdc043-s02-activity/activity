package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;


public class S2A2 {
    public static void  main(String[] args){

        int[] primeNumber = new int[5];

        primeNumber[0] = 2;
        System.out.println("The first prime number is: " +primeNumber[0]);

        primeNumber[1] = 3;
        System.out.println("The second prime number is: " +primeNumber[1]);

        primeNumber[2] = 5;
        System.out.println("The third prime number is: " +primeNumber[2]);

        primeNumber[3] = 7;
        System.out.println("The fourth prime number is: " +primeNumber[3]);

        primeNumber[4] = 11;
        System.out.println("The fifth prime number is: " +primeNumber[4]);

        //create an ArrayList named friends using a generic collection
        //in a generic collection, the data type of elements are specified in angular braces
        //adding of elements of a different data type will result in a compile time error
        ArrayList<String> friends = new ArrayList<String>();
        //add entries to our ArrayList
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        //output contents of ArrayList friends in the console
        System.out.println("My friends are: " + friends);

        //using generics, create a HashMap that has elements with key of data type String and value of data type integer
        HashMap<String,Integer> inventory = new HashMap<String,Integer>();
        //add elements with matching key, value pairs
        inventory.put("toothbrush", 20);
        inventory.put("toothpaste", 15);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
